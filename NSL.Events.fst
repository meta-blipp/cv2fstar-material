module NSL.Events

open CVTypes
open NSL.Types

let nsl_print_event e =
  match e with
  | Event_endA var0 var1 var2 var3 ->
    IO.print_string "endA: { ";
    print_label_bytes  "address" (serialize_address var0) true;
    print_label_bytes  "address" (serialize_address var1) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var2) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var3) true;
    IO.print_string "}"; IO.print_newline ()
  | Event_beginA var0 var1 var2 var3 ->
    IO.print_string "beginA: { ";
    print_label_bytes  "address" (serialize_address var0) true;
    print_label_bytes  "address" (serialize_address var1) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var2) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var3) true;
    IO.print_string "}"; IO.print_newline ()
  | Event_endB var0 var1 var2 var3 ->
    IO.print_string "endB: { ";
    print_label_bytes  "address" (serialize_address var0) true;
    print_label_bytes  "address" (serialize_address var1) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var2) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var3) true;
    IO.print_string "}"; IO.print_newline ()
  | Event_beginB var0 var1 var2 var3 ->
    IO.print_string "beginB: { ";
    print_label_bytes  "address" (serialize_address var0) true;
    print_label_bytes  "address" (serialize_address var1) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var2) true;
    print_label_bytes  "lbytes 8" (serialize_lbytes var3) true;
    IO.print_string "}"; IO.print_newline ()
