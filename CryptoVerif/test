#!/bin/sh

set -e
cvdir=$PWD

if [ ! -x ./analyze ]
then
    echo ERROR analyze not found
    exit 2
fi

if [ -d /local/bblanche/tmp ]
then
    TMPPARENT=/local/bblanche/tmp
else
    TMPPARENT=.
fi

TMPDIR="$(mktemp -d -p $TMPPARENT)"
TMP=$TMPDIR/tmp
timeoutopt=""

# usage message, in case of bad arguments

usage()
{
    cat <<'EOF' 
Usage: test [-timeout <n>] <mode> <test_set>
where 
-timeout <n>: sets the time out to <n> seconds
<mode> can be:
- test: test the mentioned scripts
- test_add: test the mentioned scripts and add the 
expected result in the script when it is missing
- add: add the expected result in the script when it is missing, 
do not test scripts that already have an expected result
- update: test the mentioned scripts and update the expected
result in the script
and <test_set> can be: basic, proverif, converted, big, impl, all, or
dir <prefix> <list_of_directories>
- proverif runs ProVerif on tests suitable for it
- converted runs CryptoVerif on examples converted from CryptoVerif 1.28
- all runs all tests included in basic, proverif, converted, big, and impl
- dir <prefix> <list_of_directories> analyzes the mentioned directories
using CryptoVerif, using <prefix> as prefix for the output files.
EOF
    exit 2
}


# analyzedirlist <prefix> <list of directories>
# analyzes the CryptoVerif scripts in the given list of directories,
# and outputs the results in tests/<prefix>`date`
# with summary in tests/sum-<prefix>`date`
# and comparison with expected results on the standard output and in tests/res-<prefix>`date`

analyzedirlist()
{
    prefix=$1
    shift
    for dir in "$@"
    do
	if [ -d "$dir" ]
	then
	    if [ -x "$dir/prepare" ]
	    then
		cd "$dir"
		./prepare
		cd "$cvdir"
	    fi
	fi
    done
    ./analyze $timeoutopt CV "$mode" "$TMPDIR" "$prefix" dirs "$@"
}

# The functions basic, proverif, converted, and big run the various
# available tests.

basic()
{
    analyzedirlist test examples/basic examples/obasic examples/prf-odh examples/kerberos examples/hpke implementation/wlsk implementation/nspk

    if [ -d examplesnd ]
    then
	analyzedirlist ndtest examplesnd/test examplesnd/proverif examplesnd/otest
    fi
}

proverif()
{
    echo
    echo PROVERIF
    echo
    ./analyze PV "$mode" "$TMPDIR" pv dirs examples examplesnd/proverif
}

converted()
{
    if [ -d examples-1.28-converted ]
    then
	analyzedirlist conv examples-1.28-converted
    fi
}

big()
{
    analyzedirlist textsecure examples/textsecure examplesnd/textsecure-2 examplesnd/textsecure examplesnd/textsecure-keys-not-normalized examplesnd/textsecure-X448 examplesnd/textsecure-single_coord_ladder

    # The TLS conference version is tested only as a converted example.
    analyzedirlist tlsx examples/tls13

    analyzedirlist arinc examples/arinc823
    
    if [ -d ../dev/projects/2014ANRAirbus/ICAO9880-May2017/computational ]
    then
	dir=../dev/projects/2014ANRAirbus/ICAO9880-May2017/computational
    else
	dir=$HOME/dev/projects/2014ANRAirbus/ICAO9880-May2017/computational
    fi
    if [ -d "$dir" ]
    then
	analyzedirlist icao9880-May17- "$dir"
    fi

    analyzedirlist ssh implementation/ssh

    analyzedirlist WG examples/wireguard
}


impl()
{
    echo
    echo Implementation
    echo
    output=impl`date '+%Y.%m.%d-%H_%M_%S'`
    (
    for i in examplesnd/impl/*
    do
	echo -n PROTOCOL $i " "
	cd $i
	./build.sh > $cvdir/$TMP.out 2>&1
	cd $cvdir
	echo PROTOCOL $i >> tests/$output
	cat $TMP.out >> tests/$output
	egrep \(CORRECT\|ERROR\) $TMP.out
    done
    ) | tee tests/res-$output
}

mkdir -p tests

case X$1 in
    X-timeout) timeoutopt="-timeout $2";
               shift
               shift;;
    *)  ;;
esac

case X$1 in
    X)  mode=test;;
    Xtest|Xtest_add|Xadd|Xupdate)
	mode=$1
	shift;;
    *)  echo "Error: unknown mode $1."
	usage;;
esac

case X$1 in
    X|Xbasic)
	# test
	basic;;
    Xproverif)
	# test proverif
	proverif;;
    Xconverted)
	# test converted
	converted;;
    Xbig)
	# test big
	big;;
    Ximpl)
	impl;;
    Xall)
	# test all  (runs all tests)
	basic
	proverif
	converted
	big
	impl;;
    Xdir)
	# test dir <prefix> <list of directories>
	# analyzes the CryptoVerif scripts in the given list of directories,
	# and outputs the results in tests/<prefix>`date`
	shift
	analyzedirlist "$@";;
    *)  echo "Unknown analysis set $1"
	usage
esac

rm -r "$TMPDIR"
