(***********************************************************************
  The following is part of boolean_choice.cvl
  inspired by some CryptoVerif examples and Bruno Blanchet
***********************************************************************)

def boolean_choice(value_t, test) {

fun test(bool, value_t, value_t) : value_t.

equation forall x:value_t, y:value_t; test(true, x, y) = x.
equation forall x:value_t, y:value_t; test(false, x, y) = y.
(* Knowing the equations defined above, this can be deduced, but
   CryptoVerif can't do this on its own. *)
equation forall x:value_t, b:bool; test(b,x,x) = x.

}

(* Zero needs to be defined already, typically by the AEAD scheme that's
 * expanded somewhere before.
 *)
def boolean_choice_for_encryption(value_t, Zero, test) {

expand boolean_choice(value_t, test).

(* Knowing the equations defined above, this can be deduced, but
   CryptoVerif can't do this on its own. *)
equation forall x:value_t, y:value_t, b:bool; Zero(test(b,x,y)) = test (b,Zero(x),Zero(y)).

}
