(* OAEP scheme, proof of CPA security.
   See OAEP.ocv for the proof of CCA2 security. *)

proof {
      crypto rom(H);
      crypto rom(G);
      show_game occ;
      insert_event bad1 32;
      show_game occ;
      insert_event bad2 60;
      auto
      (* The rest of the proof works automatically with priority 2 
      for pkgen(r) in ow(f).
      crypto remove_xor(xorDr) r_5;
      crypto remove_xor(xorDow) r_7;      
      remove_assign binder pk; 
      show_game;
      crypto pd_ow(f) r_2 r_8 r_9;
      success *)
}

param qS.

type pkey [bounded].
type skey [bounded].
type seed [large,fixed].
type D [fixed,large].
type Dow [fixed,large].
type Dr [fixed,large].

(* Set partial-domain one-way trapdoor permutation *)

proba P_PD_OW.

expand set_PD_OW_trapdoor_perm(seed, pkey, skey, D, Dow, Dr, pkgen, skgen, f, invf, concat, P_PD_OW).

(* Hash functions, random oracle model *)

type hashkey [fixed].

expand ROM_hash_large(hashkey, Dr, Dow, G, hashoracleG, qG).

expand ROM_hash_large(hashkey, Dow, Dr, H, hashoracleH, qH).

(* concatenation *)
type Dm.
type Dz [large].

fun concatm(Dm,Dz):Dow [data].
const zero: Dz.

(* Xor *)

expand Xor(Dow, xorDow, zeroDow).
expand Xor(Dr, xorDr, zeroDr).

(* Implementing a test as a function.
   Useful to avoid expanding if, which is necessary for this proof. *)

fun test(bool, Dm, Dm):Dm.
equation forall x:Dm,y:Dm; test(true,x,y) = x.
equation forall x:Dm,y:Dm; test(false,x,y) = y.

(* Queries *)

query secret b1.

let processT(hkh: hashkey, hkg: hashkey, pk: pkey) = 
	OT(m1: Dm, m2: Dm) :=
	b1 <-R bool;
	(* The next line is equivalent to an "if" that will not be
	expanded. This is necessary for the system to succeed in
	proving the protocol. *)
	menc <- test(b1, m1, m2);
	r <-R Dr;
	s <- xorDow(concatm(menc, zero), G(hkg,r));
	t <- xorDr(r, H(hkh,s));
	return(f(pk, concat(s,t))).

process 
	Ogen() :=
	hkh <-R hashkey;
	hkg <-R hashkey;
	r <-R seed; 
	pk <- pkgen(r);
	sk <- skgen(r);
	return(pk);
	(run hashoracleG(hkg) | run hashoracleH(hkh) | run processT(hkh, hkg, pk))


(* EXPECTED
All queries proved.
0.124s (user 0.112s + system 0.012s), max rss 17048K
END *)
