module NSL.Initiator

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol



let oracle_initiator_finish state sid  =
match get_and_remove_session_entry state Oracle_initiator_finish sid with
| state, None -> state, None
| state, Some (se:nsl_session_entry) ->
  match se with
    | R1_initiator_finish var_Na_3 var_Nb_1 var_addrA_1 var_addrX_1 var_trustX_2 ->


          begin
            
            
            if var_trustX_2 then
            begin
              let ev = Event_endA var_addrA_1 var_addrX_1 var_Na_3 var_Nb_1 in
            let state = state_add_event state ev in
            
            (state, Some ())
            
            end
            else
            begin
              state, None
            end
          end
              

let oracle_initiator_send_msg_1 state input_43 input_42 =
let state, sid = state_reserve_session_id state in

      let var_addrA_1 = input_43 in
      let var_addrX_1 = input_42 in

          begin
            
            let state, v44 = NSL.Letfun.fun_initiator_send_msg_1_inner state var_addrA_1 var_addrX_1 in
            
            
            let bvar_45 = v44 in
              match inv_msg1succ bvar_45 with
              | None ->   state, None
              | Some (var_skA_2,var_pkX_4,var_trustX_2,var_Na_3,var_c1_3) ->
            
                begin
                  let sel = [R1_initiator_send_msg_3 var_Na_3 var_addrA_1 var_addrX_1 var_pkX_4 var_skA_2 var_trustX_2] in
                  let state = state_add_to_session state sid sel in
                  
                  (state, Some (sid, var_c1_3))
                  
                end
              
          end
              

let oracle_initiator_send_msg_3 state sid input_46 =
match get_and_remove_session_entry state Oracle_initiator_send_msg_3 sid with
| state, None -> state, None
| state, Some (se:nsl_session_entry) ->
  match se with
    | R1_initiator_send_msg_3 var_Na_3 var_addrA_1 var_addrX_1 var_pkX_4 var_skA_2 var_trustX_2 ->

      let var_c_3 = input_46 in


      
      let bvar_47 = (dec var_c_3 var_skA_2) in
        match inv_injbot bvar_47 with
        | None ->   state, None
        | Some (bvar_48) ->
        match inv_msg2 bvar_48 with
        | None ->   state, None
        | Some (bvar_49,var_Nb_1,bvar_50) ->
        if eq_lbytes bvar_49 var_Na_3 && eq_addr bvar_50 var_addrX_1 then
          begin
            
            let ev = Event_beginA var_addrA_1 var_addrX_1 var_Na_3 var_Nb_1 in
            let state = state_add_event state ev in
            
            let state, v51 = NSL.Letfun.fun_enc state (msg3 var_Nb_1) var_pkX_4 in
            
            
            let bvar_52 = v51 in
              match inv_ciphertext_some bvar_52 with
              | None ->   state, None
              | Some (var_c3_0) ->
            
                begin
                  let sel = [R1_initiator_finish var_Na_3 var_Nb_1 var_addrA_1 var_addrX_1 var_trustX_2] in
                  let state = state_add_to_session state sid sel in
                  
                  (state, Some (var_c3_0))
                  
                end
              
          end
        else state, None
      