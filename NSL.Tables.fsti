module NSL.Tables

open CVTypes
open NSL.Types
open State

type nsl_table_name: eqtype =
  | Table_trusted_keys
  | Table_all_keys

noeq type nsl_table_entry (tn:nsl_table_name) =
  | TableEntry_trusted_keys: address -> skey -> _:pkey{tn == Table_trusted_keys} -> nsl_table_entry tn
  | TableEntry_all_keys: address -> pkey -> _:bool{tn == Table_all_keys} -> nsl_table_entry tn

let nsl_table_type = TableType nsl_table_name nsl_table_entry

val nsl_print_table_entry: #tn:nsl_table_name -> nsl_table_entry tn -> FStar.All.ML unit

val nsl_tablename_to_str: nsl_table_name -> string

val nsl_print_tables: #stt:state_type{tt_of stt == nsl_table_type} -> state stt -> FStar.All.ML unit

