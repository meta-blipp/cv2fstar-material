module NSL.Functions

open CVTypes
open NSL.Types

val ciphertext_bottom: ciphertext_opt
val msg1fail: msg1res
val skgen: lbytes 32 -> skey

val pkgen: lbytes 32 -> pkey

val enc: plaintext -> pkey -> lbytes 32 -> ciphertext_opt

val msg1succ: skey -> pkey -> bool -> lbytes 8 -> ciphertext -> msg1res

val inv_msg1succ: msg1res -> option (skey * pkey * bool * lbytes 8 * ciphertext)

val msg3: lbytes 8 -> plaintext

val inv_msg3: plaintext -> option (lbytes 8)

val msg2: lbytes 8 -> lbytes 8 -> address -> plaintext

val inv_msg2: plaintext -> option (lbytes 8 * lbytes 8 * address)

val ciphertext_some: ciphertext -> ciphertext_opt

val inv_ciphertext_some: ciphertext_opt -> option (ciphertext)

val dec: ciphertext -> skey -> option bytes

val msg1: lbytes 8 -> address -> plaintext

val inv_msg1: plaintext -> option (lbytes 8 * address)

val injbot: plaintext -> option bytes

val inv_injbot: option bytes -> option (plaintext)

val kp: pkey -> skey -> keypair

val inv_kp: keypair -> option (pkey * skey)

