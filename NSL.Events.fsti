module NSL.Events

open CVTypes
open NSL.Types
open State

noeq type nsl_event =
  | Event_endA: address -> address -> lbytes 8 -> lbytes 8 -> nsl_event
  | Event_beginA: address -> address -> lbytes 8 -> lbytes 8 -> nsl_event
  | Event_endB: address -> address -> lbytes 8 -> lbytes 8 -> nsl_event
  | Event_beginB: address -> address -> lbytes 8 -> lbytes 8 -> nsl_event

val nsl_print_event: nsl_event -> FStar.All.ML unit